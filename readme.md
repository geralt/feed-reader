## Installation

tested environment: php 7.2, mysql 5.7

- composer install
- php artisan migrate
- php artisan db:seed
- npm i

For starting feeds parsing use <i>php artisan process-feeds</i> command
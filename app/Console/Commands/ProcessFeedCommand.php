<?php

namespace App\Console\Commands;

use App\Http\Services\Feed\FeedService;
use Illuminate\Console\Command;

class ProcessFeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process-feeds';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'save and process amazon and ebay feeds';

    /**
     * Service which will be process the feeds
     * @var FeedService
     */
    private $feedService;
    /**
     * Feeds list
     * @var array
     */
    private $feeds;
    /**
     * Path where we save the feeds
     * @var string
     */
    private $savePath;
    /**
     * Create a new command instance.
     * ProcessFeedCommand constructor.
     * @param FeedService $feedService
     */

    public function __construct(FeedService $feedService)
    {
        parent::__construct();
        $this->feedService = $feedService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);
        $this->savePath = $this->getSavePath();
        $this->feeds = $this->getFeedList();
        $this->storeFeeds();
        $this->info('Processing started...');
        $this->readFeeds();
        $this->info('Done');
    }

    private function getFeedList()
    {
        return config('feeds.list');
    }

    private function getSavePath()
    {
        return config('feeds.savePath');
    }

    private function storeFeeds()
    {
        foreach ($this->feeds as $key => $urls) {
            $i = 0;
            foreach ($urls as $url) {
                $feed = $this->feedService->getFeed($url);
                $this->info('Got ' . $url . ' feed. Saving...');
                $this->feedService->saveFeed($this->savePath . '/' . $key . $i, $feed);
                $this->info('Saved!');
                $i++;
            }
        }
    }

    private function readFeeds()
    {
        $files = scandir($this->savePath);
        foreach ($files as $file) {
            if($file !== '.' && $file !== '..' && $file !== '.gitignore') {
                $this->feedService->readFeed($this->savePath . '/' . $file);
            }
        }
    }
}

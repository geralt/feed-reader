<?php

namespace App\Console\Commands;

use App\Repositories\DealRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteOldDealsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete-deals';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete old deals';

    private $dealRepository;

     /**
     * Create a new command instance.
     * DeleteOldDealsCommand constructor.
     * @param DealRepository $dealRepository
     */

    public function __construct(DealRepository $dealRepository)
    {
        parent::__construct();
        $this->dealRepository = $dealRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        set_time_limit(0);
        $deals = $this->dealRepository->get();
        foreach ($deals as $deal) {
            if($this->isOld($deal)) {
                $this->dealRepository->delete($deal->id);
            }
        }
    }

    private function isOld($deal)
    {
        $end = Carbon::now();
        return $end->diffInDays($deal->updated_at) > 30;
    }
}

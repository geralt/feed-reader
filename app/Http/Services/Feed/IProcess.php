<?php

namespace App\Http\Services\Feed;

interface IProcess
{
    public function readFeed($document);
    public function processAmazonFeed($feed, $domDocument);
    public function processEbayFeed($feed, $domDocument);

}

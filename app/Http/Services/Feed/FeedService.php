<?php

namespace App\Http\Services\Feed;

use App\Http\Services\FileUploadService;
use App\Http\Services\XML\XMLReaderService;
use App\Models\Shop;
use App\Repositories\DealImageRepository;
use App\Repositories\DealRepository;
use App\Repositories\ImageRepository;
use App\Repositories\ShopRepository;

class FeedService implements IRetrieve, IProcess
{
    /**
     * Data access
     * @var DealRepository
     * @var ShopRepository
     * @var ImageRepository
     * @var DealImageRepository
     * @var XMLReaderService
     * @var FileUploadService
     */
    private $dealRepository;
    private $shopRepository;
    private $imageRepository;
    private $dealImageRepository;
    private $xmlReaderSercice;
    private $fileUploadService;

    private $amazonDeals;
    private $ebayDeals;

    public function __construct(
        XMLReaderService $XMLReaderService,
        DealRepository $dealRepository,
        ShopRepository $shopRepository,
        ImageRepository $imageRepository,
        DealImageRepository $dealImageRepository,
        FileUploadService $fileUploadService
    )
    {
        $this->dealRepository = $dealRepository;
        $this->shopRepository = $shopRepository;
        $this->imageRepository = $imageRepository;
        $this->dealImageRepository = $dealImageRepository;
        $this->xmlReaderSercice = $XMLReaderService;
        $this->fileUploadService = $fileUploadService;

        $this->amazonDeals = $this->dealRepository->getAmazonDeals();
        $this->ebayDeals = $this->dealRepository->getEbayDeals();
    }

    public function getFeed($url)
    {
        return file_get_contents($url);
    }

    public function saveFeed($name, $data)
    {
        return file_put_contents($name, $data);
    }

    public function readFeed($file)
    {
        $xml = $this->xmlReaderSercice->openXMLDocument($file);
        $dom = $this->xmlReaderSercice->createDOMDocument();

        if($this->isAmazonFeed($file)) {
            $this->processAmazonFeed($xml, $dom);
        } else {
            $this->processEbayFeed($xml, $dom);
        }
    }

    public function processAmazonFeed($feed, $dom)
    {
        while ($feed->read()) {
            if($feed->name === 'item') {
                $node = simplexml_import_dom($dom->importNode($feed->expand(), true));
                if($node->count() > 0) {
                    $description = $this->readAmazonDescription($node->description);
                    $data = [
                        'title' => $node->title,
                        'description' => '',
                        'shop_id' => Shop::AMAZON_ID,
                        'original_price' => $description['originalPrice'],
                        'discounted_price' => $description['discountedPrice'],
                        'external_id' => $description['externalId'],
                        'geo' => ''
                    ];
                    $dealId = $this->getDealId($data['external_id'], $this->amazonDeals);
                    if($dealId) {
                        $deal = $this->dealRepository->find($dealId);
                        $this->dealRepository->update($data, $deal->id);
                        if(!in_array($description['image'], $deal->getImagesOriginalNames())) {
                            $this->saveImage($description['image'], $deal->id);
                        }
                    } else {
                        $deal = $this->dealRepository->create($data);
                        $this->saveImage($description['image'], $deal->id);
                    }
                }
            }
        }
    }

    public function processEbayFeed($feed, $dom)
    {
        while ($feed->read()) {
            if ($feed->name === 'item') {
                $node = simplexml_import_dom($dom->importNode($feed->expand(), true));
                if($node->count() > 0) {
                    $data = [
                        'title' => $node->title,
                        'description' => '',
                        'shop_id' => Shop::EBAY_ID,
                        'original_price' => $node->originalPrice,
                        'discounted_price' => $node->price,
                        'external_id' => $node->itemId,
                        'geo' => ''
                    ];
                    $dealId = $this->getDealId($data['external_id'], $this->ebayDeals);
                    if($dealId === true) {
                        $deal = $this->dealRepository->find($dealId);
                        $deal->update($data, $deal->id);
                        if(!in_array($node->image225, $deal->getImagesOriginalNames())) {
                            $this->saveImage($node->image225, $deal->id);
                        }
                    } else {
                        $deal = $this->dealRepository->create($data);
                        $this->saveImage($node->image225, $deal->id);
                    }
                }
            }
        }
    }

    private function isAmazonFeed($filename)
    {
        return strpos($filename, 'amazon') !== false;
    }

    private function readAmazonDescription($description)
    {
        $data = [
            'originalPrice' => 0,
            'discountedPrice' => 0,
            'image' => '',
            'externalId' => ''
        ];
        if($description->__toString()) {
            $dom = $this->xmlReaderSercice->createDOMDocument();
            /*
             * because of feed contents special characters that haven't been converted to HTML (in a tag)
             */
            $internalErrors = libxml_use_internal_errors(true);
            $dom->loadHTML($description);
            libxml_use_internal_errors($internalErrors);

            $items = $dom->getElementsByTagName('td');
            foreach ($items as $item) {
                $text = $item->textContent;
                if(strpos($text, 'List Price') !== false) {
                    $data['originalPrice'] = $this->getPrice($text);
                }
                if(strpos($text, 'Deal Price') !== false) {
                    $data['discountedPrice'] = $this->getPrice($text);
                }
                /*
                 * Save ExternalId (only one a in feed, so we can do so)
                 */
                $links = $item->getElementsByTagName('a');
                foreach ($links as $link) {
                    $elements = parse_url($link->getAttribute('href'));
                    $parts = explode('/', $elements['path']);
                    $ref = $parts[count($parts) - 1];
                    $data['externalId'] = str_replace('ref=xs_gb_rss_', '', $ref);
                }
                /*
                 * Save Images (only one image in feed)
                 */
                $images = $item->getElementsByTagName('img');
                foreach ($images as $image) {
                    $data['image'] = $image->getAttribute('src');
                }
            }
        }
        return $data;
    }


    private function getDealId($needle = '', array $array) {
        $id = false;
        foreach ($array as $key => $value) {
            if ($value['external_id'] === $needle) {
                $id = $value['id'];
            }
        }
        return $id;
    }

    private function getPrice($text)
    {
        return (double)preg_replace('/[^0-9\.]/', '', $text);
    }

    private function saveImage($imageUrl, $dealId)
    {
        $endPos = strpos($imageUrl, '?set_id');
        if($endPos !== false) {
            $imageUrl = substr($imageUrl, 0, $endPos);
        }
        list($imagePath, $originalName) = $this->fileUploadService->storeImage($imageUrl);
        $image = $this->imageRepository->create([
            'path' => $imagePath,
            'original_name' => $originalName
        ]);
        $this->dealImageRepository->create([
            'deal_id' => $dealId,
            'image_id' => $image->id
        ]);
    }
}
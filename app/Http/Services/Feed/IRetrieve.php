<?php

namespace App\Http\Services\Feed;

interface IRetrieve
{
    public function getFeed($url);
    public function saveFeed($name, $data);
}
<?php

namespace App\Http\Services\XML;

class XMLReaderService
{
    public function openXMLDocument($path)
    {
        $xml = new \XMLReader;
        $xml->open($path, 'utf-8', LIBXML_COMPACT);
        return $xml;
    }

    public function createDOMDocument()
    {
        return new \DOMDocument;
    }

    public function createXPath($doc)
    {
        return new \DOMXPath($doc);
    }
}
<?php

namespace App\Http\Services\XML;

interface IReader
{
    public function openXMLDocument($path);
    public function createDOMDocument();
    public function createXPath($doc);
}
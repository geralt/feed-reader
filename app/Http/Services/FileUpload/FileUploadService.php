<?php

namespace App\Http\Services;

use App\Http\Services\FileUpload\IFileUpload;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;

class FileUploadService implements IFileUpload
{
    const DIRECTORY_PERMISSIONS = 0755;

    /**
     * Takes url and creates an image on server
     * @param string $image
     * @return bool|array
     */
    public function storeImage(string $image)
    {
        if($image)
        {
            try {
                $originalName = $this->getOriginalNameFromUrl($image);
                $name = str_random().'.' . $this->getOriginalExtension($originalName);
                $dirName = substr($name, 0, 2);
                $this->createDirIfNotExists($dirName);
                Image::make($image)->save(public_path("images/$dirName/").$name);
                return [$dirName . '/' . $name, $originalName];
            } catch (NotReadableException $e) {
                Log::info($e->getMessage());
            }

        }
        return false;
    }

    /**
     * @param string $name
     */
    private function createDirIfNotExists(string $name)
    {
        if(!file_exists(public_path('images'))) {
            mkdir(public_path('images'), self::DIRECTORY_PERMISSIONS);
        }
        if(!file_exists(public_path('images/' . $name))) {
            mkdir(public_path('images/' . $name), self::DIRECTORY_PERMISSIONS);
        }
    }

    private function getOriginalExtension($filename) {
        return pathinfo($filename, PATHINFO_EXTENSION);
    }

    private function getOriginalNameFromUrl($url)
    {
        $parts = explode('/', $url);
        return $parts[count($parts) - 1];
    }
}

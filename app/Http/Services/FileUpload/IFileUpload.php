<?php

namespace App\Http\Services\FileUpload;

interface IFileUpload
{
    public function storeImage(string $image);
}
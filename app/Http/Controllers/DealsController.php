<?php

namespace App\Http\Controllers;

use App\Repositories\DealRepository;
use Illuminate\Http\Request;

class DealsController extends Controller
{
    private $repository;

    public function __construct(DealRepository $dealRepository)
    {
        $this->repository = $dealRepository;
    }

    public function index(Request $request)
    {
        if($request->shop_id) {
            $deals = $this->repository
                ->where('original_price', '>', 0)
                ->where('shop_id', '=', (int)$request->shop_id)
                ->paginate()
                ->appends($request->except('page'));
        } else {
            $deals = $this->repository->where('original_price', '>', 0)->paginate();
        }
        return view('deals/index', ['deals' => $deals]);
    }
}
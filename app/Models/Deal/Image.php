<?php

namespace App\Models\Deal;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public $timestamps = false;
    public $table = 'deals_images';

    protected $fillable = [
        'deal_id', 'image_id'
    ];

    protected $hidden = [

    ];
}

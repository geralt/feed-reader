<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'path', 'original_name'
    ];

    protected $hidden = [

    ];
}

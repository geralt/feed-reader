<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    public $timestamps = false;

    const AMAZON_ID = 1;
    const EBAY_ID = 2;

    protected $fillable = [
        'title'
    ];

    protected $hidden = [

    ];
}

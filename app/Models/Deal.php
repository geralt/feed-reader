<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    protected $fillable = [
        'title', 'description', 'shop_id', 'original_price', 'discounted_price', 'external_id', 'geo'
    ];

    protected $hidden = [

    ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $this->cutTitle($value);
    }

    public function setOriginalPriceAttribute($value)
    {
        $this->attributes['original_price'] = (double)$value;
    }

    public function setDiscountedPriceAttribute($value)
    {
        $this->attributes['discounted_price'] = (double)$value;
    }

    public function images()
    {
        return $this->hasManyThrough(
            Image::class,
            Deal\Image::class,
            'deal_id',
            'id',
            'id',
            'image_id'
        );
    }

    public function shop()
    {
        return $this->belongsTo(Shop::class, 'shop_id', 'id');
    }

    public function getImagesOriginalNames()
    {
        return $this->images()
            ->select('original_name')
            ->get()
            ->toArray();
    }

    private function cutTitle($title)
    {
        if(strlen($title) > 255) {
            return substr($title, 0, 252) . '...';
        }
        return $title;
    }
}

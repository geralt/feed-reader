<?php

namespace App\Repositories;

use App\Models\Deal\Image;

class DealImageRepository extends Repository
{
    public function model()
    {
        return Image::class;
    }
}
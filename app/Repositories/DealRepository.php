<?php

namespace App\Repositories;

use App\Models\Deal;
use App\Models\Shop;

class DealRepository extends Repository
{
    public function model()
    {
        return Deal::class;
    }

    public function getAmazonDeals()
    {
        return $this->where('shop_id', '=', Shop::AMAZON_ID)
            ->get()
            ->toArray();
    }

    public function getEbayDeals()
    {
        return $this->where('shop_id', '=', Shop::EBAY_ID)
            ->get()
            ->toArray();
    }
}
<?php

namespace App\Repositories;

use App\Models\Shop;

class ShopRepository extends Repository
{
    public function model()
    {
        return Shop::class;
    }
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default('');
            $table->string('description')->default('');
            $table->integer('shop_id')->unsigned()->default(1);
            $table->foreign('shop_id')->references('id')->on('shops')->onDelete('cascade');
            $table->decimal('original_price')->default(0.00);
            $table->decimal('discounted_price')->default(0.00);
            $table->string('external_id')->default('');
            $table->string('geo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}

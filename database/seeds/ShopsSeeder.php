<?php

use Illuminate\Database\Seeder;

class ShopsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Shop::firstOrCreate([
            'title' => 'Amazon',
        ]);

        \App\Models\Shop::firstOrCreate([
            'title' => 'eBay',
        ]);
    }
}

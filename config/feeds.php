<?php

return [
    'savePath' => storage_path('/feeds'),
    'list' => [
        'amazon'    => [
            'https://rssfeeds.s3.amazonaws.com/goldbox'
        ],
        'ebay'      => [
            'http://www.ebay.com/rps/feed/v1.1/epnexcluded/EBAY-US?limit=200',
            'http://www.ebay.com/rps/feed/v1.1/epnexcluded/EBAY-US?limit=200&offset=200'
        ]
    ]
];